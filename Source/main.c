/*
 * Timer.c
 *
 * Created: 22/02/2021 11:43:41
 * Author : emuem
 */

#include "HtlStddef.h"
#include "Timer.h"
#include <avr/io.h>

int main(void) {
  TTimer timer = TimerInit(F_CPU, TIMER_ID_0, TIMER_MODE_NORMAL,
                           TIMER_CLOCK_SELECT_PRESCALER_1);
  /* Replace with your application code */
  while (1) {
  }
}

