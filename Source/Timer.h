#ifndef TIMER_H
#define TIMER_H

#include <avr/io.h>
#include <stdlib.h>

typedef struct Timer* TTimer;

typedef enum {
  TIMER_MODE_NORMAL,
} TTimerMode;

typedef enum {
  TIMER_ID_0 = 0,
  TIMER_ID_1 = 1,
  TIMER_ID_2 = 2,
} TTImerId;

/**
 * see data sheet: Table 16-10.?Clock Select Bit Description
 */
typedef enum {
  TIMER_CLOCK_SELECT_NONE = 0,
  TIMER_CLOCK_SELECT_PRESCALER_1 = 1,
  TIMER_CLOCK_SELECT_PRESCALER_8 = 2,
  TIMER_CLOCK_SELECT_PRESCALER_64 = 3,
  TIMER_CLOCK_SELECT_PRESCALER_256 = 4,
  TIMER_CLOCK_SELECT_PRESCALER_1024 = 5,
  TIMER_CLOCK_SELECT_EXTERNAL_FALLING_EDGE = 6,
  TIMER_CLOCK_SELECT_EXTERNAL_RISING_EDGE = 7,
} TTimerClockSelect;

TTimer TimerInit(unsigned long cpuClk,
                 TTImerId timerId,
                 TTimerMode mode,
                 TTimerClockSelect clockSelect);

void TimerDestroy(TTimer timer);

#endif
