#include "Timer.h"

struct Timer {
  TTimerMode mode;
  TTImerId id;
  TTimerClockSelect clockSelect;
};

static TTimer globalTimers[TIMER_ID_2 + 1] = {NULL, NULL, NULL};

/******************** Private Function Declarations ********************/
TTimer TimerInitId0(unsigned long cpuClk,
                    TTimerMode mode,
                    TTimerClockSelect clockSelect);

TTimer TimerInitId1(unsigned long cpuClk, TTimerMode mode);

TTimer TimerInitId2(unsigned long cpuClk, TTimerMode mode);

/******************** Public Function Definitions ********************/
TTimer TimerInit(unsigned long cpuClk,
                 TTImerId timerId,
                 TTimerMode mode,
                 TTimerClockSelect clockSelect) {
  TTimer timer = NULL;

  if (NULL != globalTimers[timerId])
    return timer;

  switch (timerId) {
    case TIMER_ID_0:
      timer = TimerInitId0(cpuClk, mode, clockSelect);
      break;

    case TIMER_ID_1:
      timer = TimerInitId1(cpuClk, mode);
      break;

    case TIMER_ID_2:
      timer = TimerInitId2(cpuClk, mode);
      break;

    default:
      timer = NULL;
  }

  globalTimers[timerId] = timer;

  return timer;
}

void TimerDestroy(TTimer timer) {}

/******************** Private Function Definitions ********************/
TTimer TimerInitId0(unsigned long cpuClk,
                    TTimerMode mode,
                    TTimerClockSelect clockSelect) {
  TTimer timer = (TTimer)calloc(1, sizeof(struct Timer));

  timer->clockSelect = clockSelect;
  timer->id = TIMER_ID_0;
  timer->mode = mode;

  TCCR0A = 0;
  TCCR0B = 0;
  TCCR0B |= (clockSelect << CS00);

  return timer;
}

TTimer TimerInitId1(unsigned long cpuClk, TTimerMode mode) {
  return NULL;
}

TTimer TimerInitId2(unsigned long cpuClk, TTimerMode mode) {
  return NULL;
}
